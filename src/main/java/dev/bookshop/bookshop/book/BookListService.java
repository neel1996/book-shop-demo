package dev.bookshop.bookshop.book;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class BookListService {
    @Autowired
    BookListDataRepository bookListDataRepository;

    public List<Book> listAllBooks() {
        List<Book> booksList = new ArrayList<>();
        bookListDataRepository.findAll().forEach(booksList::add);
        return booksList;
    }

    public void addNewBook(List<Book> book) {
        bookListDataRepository.saveAll(book);
    }
}
