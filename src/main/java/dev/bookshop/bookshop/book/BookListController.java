package dev.bookshop.bookshop.book;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class BookListController {

    final
    BookListService bookListService;

    public BookListController(BookListService bookListService) {
        this.bookListService = bookListService;
    }

    @GetMapping(path = "/listbooks", produces = "application/json")
    public List<Book> listBooks() {
        return bookListService.listAllBooks();
    }

}
