package dev.bookshop.bookshop.book;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface BookListDataRepository extends CrudRepository<Book, Integer> {
}
