package dev.bookshop.bookshop;

import dev.bookshop.bookshop.book.Book;
import dev.bookshop.bookshop.book.BookListDataRepository;
import dev.bookshop.bookshop.book.BookListService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.Mockito.when;

public class BookShopListControllerTest {

    @InjectMocks
    BookListService bookListService;

    @Mock
    BookListDataRepository bookListDataRepository;

    @BeforeEach
    void setupBeforeEachTest() {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    void shouldFetchTheListOfBooksAndTheListMustNotBeEmpty() {
        Book book = new Book();
        book.setBookId(1);
        book.setBookName("Test");
        book.setAuthor("Test");
        book.setYearOfPublishing(2001);

        List<Book> books = new ArrayList<>();
        books.add(book);

        when(bookListDataRepository.findAll()).thenReturn(books);
    }

}
